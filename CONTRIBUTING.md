# Contributing

If you wish to contribute, you must follow the existing style. To achieve it, respect the
existing [Editor Configuration](.editorconfig) configuration and use CSharpier (dotnet-csharpier) to format your files.

Ensure each class has its imports with the same name of the class followed by "Imports", i.e.: AsyncQueue ->
AsyncQueueImports.

Respect the existing [Gnome.Net.Common](README.md#gnomenetcommon) tooling and make use of it, don't remake its
functionality somewhere else!

Each GNOME related library must have their own project and link to dependencies accordingly.

For every `LibraryImport` attribute, there MUST be a `UnmanagedCallConventions` attribute that is
exactly: `[UnmanagedCallConventions = new [] { typeof(CallConvCdecl) }]`.

Make use of the existing Microsoft custom marshaller classes first, create custom marshaller classes as a backup for
when you cannot use the existing Microsoft ones.

No `DllImport` attributes are allowed, this project makes heavy use of P/Invoke code marshalling instead.
