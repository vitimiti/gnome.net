# Gnome.Net

A marshalled modern GTK stack set of libraries to use the GNOME system through modern dotnet.

Table of Contents:
- [List of Projects](#list-of-projects)
  - [Gnome.Net.Common](#gnomenetcommon)
  - [Gnome.Net.GLib](#gnomenetglib)
    - [Related Libraries](#related-libraries)
    - [Native Dependencies](#native-dependencies)
  - [QuickTests](#quicktests)
- [Contributing](#contributing)
- [License](#license)
- [Contact Information](#contact-information)
- [Changelog](#changelog)

## List of Projects

### Gnome.Net.Common

A common library for all Gnome.Net projects to include common utilities and tools.

Currently, this library does:

- Setup the library name depending on OS and native library.
- Create a DllImporter function to share amongst all projects.

### Gnome.Net.GLib

GLib is a general-purpose, portable utility library, which provides many useful data types, macros, type conversions,
string utilities, file utilities, a mainloop abstraction, and so on.

- Version: 2.76
- Authors: GTK Development Team
- License: LGPL-2.1-or-later
- [Website](https://www.gtk.org)
- [Source](https://gitlab.gnome.org/GNOME/glib/)

#### Related Libraries

- [Gnome.Net.GModule](#gnomenetgmodule)
- [Gnome.Net.GObject](#gnomenetgobject)
- [Gnome.Net.GIo](#gnomenetgio)

#### Native Dependencies

- Windows
    - libglib-2.0-0.dll
- OSX
    - libglib-2.0.0.dylib
- Linux
    - libglib-2.0.so.0
- If other platform, the default library name will be the same as in Linux.

### QuickTests

A quick tests project that is empty. This project WILL BE REMOVED once all the libraries are implemented in their most
basic form and will always be empty and without usable code.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## License

The MIT license is in use in this project. See the [license file](LICENSE.md) for more information.

## Contact Information

- Repository owners:
  - Victor Matia <vmatir@gmail.com>.

## Changelog

See [CHANGELOG.md](CHANGELOG.md) for more information.
