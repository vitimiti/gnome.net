// This file is part of the Gnome.Net project and is under the MIT license.
// See LICENSE.md for more information.

namespace Gnome.Net.Common;

/// <summary>
///     Represents a library name resolver that provides functionality to resolve platform-dependent library names.
/// </summary>
public struct LibraryName
{
    private static readonly Dictionary<string, bool> GLibNames =
        new()
        {
            { "libglib-2.0-0.dll", OperatingSystem.IsWindows() },
            { "libglib-2.0.0.dylib", OperatingSystem.IsMacOS() },
            { "libglib-2.0.so.0", OperatingSystem.IsLinux() }
        };

    /// <summary>Represents the default filename of the Glib library.</summary>
    public const string GLib = "libglib-2.0.so.0";

    internal static string GetOsVersionDependentLibraryName(string libraryName)
    {
        switch (libraryName)
        {
            case GLib:
                foreach (var name in GLibNames.Where(name => name.Value))
                {
                    return name.Key;
                }

                break;

            default:
                return libraryName;
        }

        return libraryName;
    }
}
